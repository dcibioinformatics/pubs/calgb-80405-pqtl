wd=<path to directory where the defintion file (dcibioinformatcsR.def) is>
sifdir=<path to directory where you want the output image (*.sif) to go>
version=2.2
tm=$$(date '+%Y-%m-%d_%H-%M-%S')

build:
	time singularity build \
		--fakeroot \
		${sifdir}/dcibioinformaticsR-v${version}.sif \
		dcibioinformaticsR.def > ${wd}/buildfull_${version}_${tm}.log 2>&1
