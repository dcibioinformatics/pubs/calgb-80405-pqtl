## CALGB80405 pQTL

### Reproduce preprocessing and analysis

Build singularity container for reproducing preprocessing and analysis results:

1. Update `wd` and `sifdir` in the `Makefile` under the top-level of `Code/`
2. Build container by running the following command under `Code/`

```
make build
```

Run preprocessing and analysis scripts in the order listed below (these are designed to be run **interactively**).  

`Code/01_preprocessing_CALGB80405_pQTL.Rmd`

Reads in VCF files for batch 1 and batch 2 of the genotyping data. Converts VCFs to GDS format for subsetting and merging using `SeqArray` -- note that the text file containing the RSIDs to subset the GDS objects by is under `Code/CALGB80405_b1b2_n540021_analysis_variantslist.txt`. Global ancestry is inferred for genetic Europeans using principal components analysis and finally merged data is subset by patients with protein data.

`Code/02_fastJT_CALGB80405_pQTL.Rmd`

Runs fastJT package to obtain the JT statistics for all 23 marker x 540021 SNP combinations and saves the results.

`Code/03_Rfit_CALGB80405_pQTL.Rmd`

Reads in JT test results, runs rank-based linear regression on the top hits using Rfit package, and saves the results. Peforms LD analysis for select SNP pairs mentioned in manuscript.

### Tables, figures, and validation

`Code/TblsFigs`
  
Code to generate the tables and figures included in the manuscript and supplementary material.

* Note: `80405-pQTL-MHQQLZ.Rnw` and `80405-pQTL-circos.Rnw` can be run without using the singularity container. `Tables.Rmd` and `Figure2.Rmd` are designed to be run interactively within the singularity container.

`Code/Validation`
  
Code to test association of rs11118119 and TGFb2 in CALGB 80303 and CALGB 90401.

To reproduce either of these reports, first update the `Makefile` under `Code/Validation` to point to the directory where your singularity image is located (`sif`) and your working directory where your input files are located (`wd`). Then render the reports by running the following under `Code/Validation`:

```
make report FILE=80303.Rmd

# or 

make report FILE=90401.Rmd
```
